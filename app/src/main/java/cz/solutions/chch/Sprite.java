/*
 * Copyright (C) 2012 Ciaran Gultnieks, ciaran@ciarang.com
 * Copyright (C) 2010 František Hejl
 * Copyright (C) 2019 David Strupl
 *
 * This file is part of Chinese Chess.
 *
 * Chinese Chess is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Chesswalk is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Chinese Chess.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package cz.solutions.chch;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PaintFlagsDrawFilter;

import java.util.ArrayList;

public class Sprite {
    public int z;
    private final Paint alphaPaint = new Paint();
    protected float baseX;
    protected float baseY;
    protected float draggedX;
    protected float draggedY;
    protected final float height;
    protected float scale = 1;
    protected final float width;
    protected int alpha = 255;
    protected final ArrayList<Animation> animations = new ArrayList<>();
    protected Bitmap bitmap;

    private final PaintFlagsDrawFilter antialiasingOn = new PaintFlagsDrawFilter(
            0, Paint.FILTER_BITMAP_FLAG);
    private final PaintFlagsDrawFilter antialiasingOff = new PaintFlagsDrawFilter(
            Paint.FILTER_BITMAP_FLAG, 0);

    public void draw(Canvas canvas, long time) {
        // update animations
        for (int i = 0; i < animations.size(); i++) {
            animations.get(i).update(this, time);
        }

        // remove finished animations
        for (int i = animations.size() - 1; i >= 0; i--) {
            if (animations.get(i).finished)
                animations.remove(i);
        }

        // draw
        canvas.setDrawFilter(antialiasingOff);
        Paint paint = null;
        if (alpha < 255) {
            paint = alphaPaint;
            paint.setAlpha(alpha);
        }
        canvas.save();
        if (scale != 1) {
            canvas.setDrawFilter(antialiasingOn);
            canvas.scale(scale, scale, baseX + draggedX, baseY + draggedY);
        }
        canvas.drawBitmap(bitmap, baseX + draggedX - width / 2, baseY
                + draggedY - height / 2, paint);
        if (ChessBoardView.debug) {
            Paint rectanglePaint = new Paint();
            rectanglePaint.setARGB(255, 255, 0, 0);
            rectanglePaint.setStrokeWidth(2);
            rectanglePaint.setStyle(Paint.Style.STROKE);
            rectanglePaint.setTextSize(48);
            canvas.drawRect(baseX - width / 2 + draggedX, baseY - height / 2 + draggedY, baseX
                    + width / 2 + draggedX, baseY + height / 2 + draggedY, rectanglePaint);
            canvas.drawText("" + z,
                    baseX + z,
                    baseY, rectanglePaint);
        }
        canvas.restore();
    }

    public Sprite(Bitmap bitmap, float x, float y, int z) {
        this.bitmap = bitmap;
        this.baseX = x;
        this.baseY = y;
        this.z = z;
        height = bitmap.getHeight();
        width = bitmap.getWidth();
    }

    public Sprite(float x, float y, float width, float height, int z) {
        this.baseX = x;
        this.baseY = y;
        this.z = z;
        this.height = height;
        this.width = width;
    }

    static class ChessPieceSprite extends Sprite {

        int file;
		int rank;

		public ChessPieceSprite(Bitmap bitmap, int file, int rank, int z, ChessBoardView view) {
			super(bitmap, view.leftOffset + file * view.squareSizeX + view.squareSizeX / 2, view.topOffset + (9 - rank) * view.squareSizeY + view.squareSizeY / 2, z);
			this.file = file;
			this.rank = rank;
		}
	}

    static class RectangleSprite extends Sprite {
		public int file = -1;
		public int rank = -1;
		private int r;
		private int g;
		private int b;
        private final Paint rectanglePaint = new Paint();

		public void draw(Canvas canvas, long time) {
			// update animations
			for (int i = 0; i < animations.size(); i++) {
				animations.get(i).update(this, time);
			}

			// remove finished animations
			for (int i = animations.size() - 1; i >= 0; i--) {
				if (animations.get(i).finished)
					animations.remove(i);
			}

			// draw
			rectanglePaint.setARGB(alpha, r, g, b);
			canvas.drawRect(baseX - width / 2, baseY - height / 2, baseX
					+ width / 2, baseY + height / 2, rectanglePaint);
		}

		public void setRGB(int r, int g, int b) {
			this.r = r;
			this.g = g;
			this.b = b;
		}

		public RectangleSprite(float x, float y, float width, float height,
							   int z) {
			super(x, y, width, height, z);
		}

	}
}
