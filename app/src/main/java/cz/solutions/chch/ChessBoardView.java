/*
 * Copyright (C) 2012 Ciaran Gultnieks, ciaran@ciarang.com
 * Copyright (C) 2010 František Hejl
 * Copyright (C) 2019 David Strupl
 *
 * This file is part of Chinese Chess.
 *
 * Chinese Chess is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Chesswalk is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Chinese Chess.  If not, see <http://www.gnu.org/licenses/>.
 *
ChessBoardView.java - Source Code for XiangQi Wizard Light, Part IV

XiangQi Wizard Light - a Chinese Chess Program for Java Applet
Designed by Morning Yellow, Version: 1.25, Last Modified: Mar. 2008
Copyright (C) 2004-2008 www.elephantbase.net
Modified by David Strupl
Copyright (C) 2019 David Strupl

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
package cz.solutions.chch;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;

@SuppressWarnings("SynchronizeOnNonFinalField")
public class ChessBoardView extends SurfaceView implements SurfaceHolder.Callback {

    public static final String TAG = "ChessBoardView";
    private static final int CHESSBOARD_WIDTH = 1020;
    private static final int CHESSBOARD_HEIGHT = 1150;
    private static final int DRAGGING_DELAY = 450;
    private static final int Z_ORDER_CHESSBOARD = 0;
    private static final int Z_ORDER_CHESS_PIECE = 5;

    public static final boolean debug = false;
    boolean flipped = false;
    private boolean pieceDragged = false;
    private boolean pieceSelected = false;
    private float draggingStartX;
    private float draggingStartY;
    final float leftOffset = 0f;
    float squareSizeX = 160f;
    float squareSizeY = 160f;
    final float topOffset = 10f;
    private long whenShouldThreadPause;
    private final ArrayList<MotionEvent> inputEvents = new ArrayList<>(20);
    private final ArrayList<Sprite> legalSquareSprites = new ArrayList<>();
    private final ArrayList<Sprite> sprites = new ArrayList<>();
    private Bitmap lastMoveHighlightBitmap;
    private Bitmap legalMoveHighlightBitmap;
    private Bitmap selectedPieceBitmap;
    private Sprite.ChessPieceSprite draggedPiece;
    private Sprite.ChessPieceSprite selectedPiece;
    private DrawingThread thread;

    private Sprite.RectangleSprite horizontalRectangle;
    private Sprite.RectangleSprite verticalRectangle;
    private Sprite selectedPieceSprite;
    private final Sprite[] lastMoveHighlights = new Sprite[2];
    private final SurfaceHolder surfaceHolder;

    private static final String[] PIECE_NAME = {
            null, null, null, null, null, null, null, null,
            "rk", "ra", "rb", "rn", "rr", "rc", "rp", null,
            "bk", "ba", "bb", "bn", "br", "bc", "bp", null,
    };

    final Bitmap[] imgPieces = new Bitmap[PIECE_NAME.length];
    Bitmap imgBoard;

    GameState state = new GameState();
    int sqSelected;
    volatile boolean thinking = false;
    int handicap = 0, level = 0;

    boolean blackComputer = false;
    boolean whiteComputer = false;
    boolean blackRemote = false;
    boolean whiteRemote = false;
    MoveListener moveListener;

    // -------------------------------------------------------------------------------------------------------

    private void addSprite(Sprite sprite) {
        for (int i = 0; i <= sprites.size(); i++) {
            if (i == sprites.size() || sprite.z >= sprites.get(i).z) {
                sprites.add(i, sprite);
                break;
            }
        }
    }

    // -------------------------------------------------------------------------------------------------------

    public ChessBoardView(Context context, AttributeSet attrs) {
        super(context, attrs);
        surfaceHolder = getHolder();
        surfaceHolder.addCallback(this);
        state.currentFen = Position.STARTUP_FEN[handicap];
        restart();
    }

    // -------------------------------------------------------------------------------------------------------

    private void doSecondaryMoveAnimation(int fromFile, int fromRank, int toFile, int toRank, long startTime) {
        Sprite.ChessPieceSprite capturedPiece = findChessPieceSprite(toFile, toRank);
        if (capturedPiece != null) {
            capturedPiece.animations.add(new Animation.ScaleAnimation(7, 1, startTime, 200));
            capturedPiece.animations.add(new Animation.AlphaAnimation(-255, 255,
                    startTime, 200));
            capturedPiece.animations.add(new RemoveAnimation(startTime, 200));
        }

        // set last move highlight
        lastMoveHighlights[0].baseX = getXFromFile(flipped ? 8 - fromFile : fromFile);
        lastMoveHighlights[0].baseY = getYFromRank(flipped ? 9 - fromRank : fromRank);
        lastMoveHighlights[0].animations.add(new Animation.AlphaAnimation(255, 0,
                startTime, 100));
        lastMoveHighlights[1].baseX = getXFromFile(flipped ? 8 - toFile : toFile);
        lastMoveHighlights[1].baseY = getYFromRank(flipped ? 9 - toRank : toRank);
        lastMoveHighlights[1].animations.add(new Animation.AlphaAnimation(255, 0,
                startTime, 100));
    }

    // -------------------------------------------------------------------------------------------------------

    private Sprite.ChessPieceSprite findChessPieceSprite(int file, int rank) {
        for (int i = 0; i < sprites.size(); i++) {
            if (sprites.get(i) instanceof Sprite.ChessPieceSprite) {
                Sprite.ChessPieceSprite chessPiece = (Sprite.ChessPieceSprite) sprites.get(i);
                if (chessPiece.file == file && chessPiece.rank == rank)
                    return chessPiece;
            }
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------------

    private int getFileFromX(int x) {
        if (flipped)
            return 8 - (int) ((x - leftOffset) / squareSizeX);
        else
            return (int) ((x - leftOffset) / squareSizeX);
    }

    // -------------------------------------------------------------------------------------------------------

    public boolean getFlipped() {
        return flipped;
    }

    // -------------------------------------------------------------------------------------------------------

    int getRankFromY(int y) {
        if (flipped)
            return (int) ((y - topOffset) / squareSizeY);
        else {
            return 9 - (int) ((y - topOffset) / squareSizeY);
        }
    }

    // -------------------------------------------------------------------------------------------------------

    float getXFromFile(int file) {
        if (flipped)
            file = 8 - file;
        return leftOffset + file * squareSizeX + squareSizeX / 2;
    }

    // -------------------------------------------------------------------------------------------------------

    float getYFromRank(int rank) {
        if (flipped) {
            rank = 9 - rank;
        }
        return topOffset + (9 - rank) * squareSizeY + squareSizeY / 2;
    }

    private void initSprites() {
        sprites.clear();

        // add chessboard sprite
        addSprite(new Sprite(imgBoard, imgBoard.getWidth() / 2f,
                imgBoard.getHeight() / 2f, Z_ORDER_CHESSBOARD));

        // rectangles
        horizontalRectangle = new Sprite.RectangleSprite(leftOffset + 4 * squareSizeX,
                0, 10 * squareSizeX, squareSizeY, 3);
        horizontalRectangle.alpha = 0;
        addSprite(horizontalRectangle);
        verticalRectangle = new Sprite.RectangleSprite(0, topOffset + 4 * squareSizeY,
                squareSizeX, 11 * squareSizeY, 3);
        verticalRectangle.alpha = 0;
        addSprite(verticalRectangle);

        // selected piece highlight
        selectedPieceSprite = new Sprite(selectedPieceBitmap, 0, 0, 4);
//
//		// last move highlights
        lastMoveHighlights[0] = new Sprite(lastMoveHighlightBitmap, 0, 0, 4);
        lastMoveHighlights[0].alpha = 0;
        addSprite(lastMoveHighlights[0]);
        lastMoveHighlights[1] = new Sprite(lastMoveHighlightBitmap, 0, 0, 4);
        lastMoveHighlights[1].alpha = 0;
        addSprite(lastMoveHighlights[1]);

        // add chess pieces
        for (int x = Position.FILE_LEFT; x <= Position.FILE_RIGHT; x++) {
            for (int y = Position.RANK_TOP; y <= Position.RANK_BOTTOM; y++) {
                //Log.d(TAG, "1 Adding piece x = " + x + " y = " + y);
                int sq = Position.COORD_XY(x, y);
                sq = (flipped ? Position.SQUARE_FLIP(sq) : sq);
                int file = x - Position.FILE_LEFT;
                int rank = y - Position.RANK_TOP;
                int pc = state.pos.squares[sq];
                if (pc > 0) {
                    //Log.d(TAG, "3 Adding piece "+ PIECE_NAME[pc] +" to file " + file + " rank " + rank);
                    addSprite(new Sprite.ChessPieceSprite(imgPieces[pc], file, rank, Z_ORDER_CHESS_PIECE, this));
                }
            }
        }
    }

    // -------------------------------------------------------------------------------------------------------

    private void inputDown(float x, float y, long currentTime) {
        //Log.d(TAG, "inputDown sdPlayer = " + pos.sdPlayer + " blackHuman = " + blackHuman);
        if (state.status > 0 || thinking) {
            Log.d(TAG, "inputDown returning 1 !!!!!");
            return;
        }
        if (state.doNotMoveHuman(whiteComputer, blackComputer, whiteRemote, blackRemote)) {
            Log.d(TAG, "inputDown returning 2 !!!!!");
            return;
        }
        if (pieceSelected) {
            pieceSelected = false;

            // remove highlight under selected piece
            selectedPieceSprite.animations.clear();
            selectedPieceSprite.animations.add(new RemoveAnimation(currentTime,
                    100));
            selectedPieceSprite.animations.add(new Animation.AlphaAnimation(
                    -selectedPieceSprite.alpha, selectedPieceSprite.alpha,
                    currentTime, 100));

            // remove legal moves highlights
            for (int i = 0; i < legalSquareSprites.size(); i++) {
                Sprite sprite = legalSquareSprites.get(i);
                sprite.animations.add(new RemoveAnimation(currentTime, 100));
                sprite.animations.add(new Animation.AlphaAnimation(
                        -selectedPieceSprite.alpha, selectedPieceSprite.alpha,
                        currentTime, 100));
            }
            //sprites.removeAll(legalSquareSprites);
            legalSquareSprites.clear();

            int file = getFileFromX((int) x);
            int rank = getRankFromY((int) y);

            if (rank >= 0 && rank <= 9 && file >= 0 && file <= 8) {
                movePiece(selectedPiece.file, selectedPiece.rank, flipped ? 8 - file : file, flipped ? 9 - rank : rank);
            } else {
                Sprite.ChessPieceSprite chessPiece = findChessPieceSprite(file, rank);
                //Log.d(TAG, "PIECE 1 " + chessPiece);
                if (chessPiece != null) {
                    selectPiece(file, rank, currentTime);
                }
            }
        } else {
            int file = getFileFromX((int) x);
            file = flipped ? 8 - file : file;
            int rank = getRankFromY((int) y);
            rank = flipped ? 9 - rank : rank;

            Sprite.ChessPieceSprite chessPiece = findChessPieceSprite(file, rank);
            if (chessPiece != null) {
                pieceDragged = true;
                draggingStartX = x;
                draggingStartY = y;
                draggedPiece = chessPiece;
                setSpriteZOrder(draggedPiece, Z_ORDER_CHESS_PIECE + 1);
                draggedPiece.animations.add(new Animation.MoveAnimation(x
                        - draggedPiece.baseX, y - 1.5f * squareSizeY
                        - draggedPiece.baseY, draggedPiece.baseX,
                        draggedPiece.baseY, currentTime + DRAGGING_DELAY
                        - 350, 100));
                draggedPiece.animations.add(new Animation.AlphaAnimation(-100, 255,
                        currentTime + DRAGGING_DELAY - 350, 100));
                draggedPiece.animations.add(new Animation.ScaleAnimation(1.5f, 1,
                        currentTime + DRAGGING_DELAY - 350, 100));
            }
        }
    }

    // -------------------------------------------------------------------------------------------------------

    private void inputMove(MotionEvent event, long currentTime) {
        //Log.d(TAG, "inputMove sdPlayer = " + pos.sdPlayer + " blackHuman = " + blackHuman);
        if (state.status > 0) {
            Log.d(TAG, "inputMove returning 1 !!!!!");
            return;
        }

        if (state.doNotMoveHuman(whiteComputer, blackComputer, whiteRemote, blackRemote)) {
            Log.d(TAG, "inputMove returning 2 !!!!!");
            return;
        }
        if (pieceDragged) {
            draggedPiece.draggedX = event.getX() - draggingStartX;
            draggedPiece.draggedY = event.getY() - draggingStartY;
            int rank = getRankFromY((int) event.getY());
            int file = getFileFromX((int) event.getX());
            if (rank != horizontalRectangle.rank
                    || file != verticalRectangle.file) {
                horizontalRectangle.rank = rank;
                verticalRectangle.file = file;
                if (file == draggedPiece.file && rank == draggedPiece.rank) {
                    horizontalRectangle.animations.clear();
                    horizontalRectangle.animations.add(new Animation.AlphaAnimation(
                            -horizontalRectangle.alpha,
                            horizontalRectangle.alpha, currentTime, 100));
                    verticalRectangle.animations.clear();
                    verticalRectangle.animations.add(new Animation.AlphaAnimation(
                            -verticalRectangle.alpha, verticalRectangle.alpha,
                            currentTime, 100));
                } else {
                    if (rank >= 0 && rank <= 9 && file >= 0 && file <= 8) {
//							&& legalSquares[rank * 16 + file]) {
                        horizontalRectangle.setRGB(0, 255, 0);
                        verticalRectangle.setRGB(0, 255, 0);
                    } else {
                        horizontalRectangle.setRGB(255, 0, 0);
                        verticalRectangle.setRGB(255, 0, 0);
                    }

                    if (horizontalRectangle.alpha == 0) {
                        horizontalRectangle.baseY = getYFromRank(rank);
                        verticalRectangle.baseX = getXFromFile(file);
                    }

                    horizontalRectangle.animations.clear();
                    horizontalRectangle.animations.add(new Animation.MoveAnimation(0,
                            getYFromRank(rank) - horizontalRectangle.baseY,
                            horizontalRectangle.baseX,
                            horizontalRectangle.baseY, currentTime, 50));
                    horizontalRectangle.animations.add(new Animation.AlphaAnimation(
                            50 - horizontalRectangle.alpha,
                            horizontalRectangle.alpha, currentTime, 50));

                    verticalRectangle.animations.clear();
                    verticalRectangle.animations.add(new Animation.MoveAnimation(
                            getXFromFile(file) - verticalRectangle.baseX, 0,
                            verticalRectangle.baseX, verticalRectangle.baseY,
                            currentTime, 50));
                    verticalRectangle.animations.add(new Animation.AlphaAnimation(
                            50 - verticalRectangle.alpha,
                            verticalRectangle.alpha, currentTime, 50));
                }
            }
        }
    }

    // -------------------------------------------------------------------------------------------------------

    private void inputUp(MotionEvent event, long currentTime) {
        //Log.d(TAG, "inputUp sdPlayer = " + pos.sdPlayer + " blackHuman = " + blackHuman);
        if (state.status > 0) {
            //Log.d(TAG, "inputUp returning  1 !!!!");
            return;
        }

        if (state.doNotMoveHuman(whiteComputer, blackComputer, whiteRemote, blackRemote)) {
            Log.d(TAG, "inputUp returning 2 !!!!!");
            return;
        }
        if (pieceDragged) {
            pieceDragged = false;
            draggedPiece.animations.clear();

            int file = getFileFromX((int) event.getX());
            int rank = getRankFromY((int) event.getY());

            float destinationX = getXFromFile(file);
            float destinationY = getYFromRank(rank);

            // hide rectangles
            horizontalRectangle.animations.clear();
            verticalRectangle.animations.clear();
            horizontalRectangle.animations.add(new Animation.AlphaAnimation(
                    -horizontalRectangle.alpha, horizontalRectangle.alpha,
                    currentTime, 100));
            verticalRectangle.animations.add(new Animation.AlphaAnimation(
                    -verticalRectangle.alpha, verticalRectangle.alpha,
                    currentTime, 100));
            horizontalRectangle.rank = -1;
            verticalRectangle.file = -1;

            if (draggedPiece.file == (flipped ? 8 - file : file)
                    && draggedPiece.rank == (flipped ? 9 - rank : rank)
                    && (SystemClock.uptimeMillis() - event.getDownTime()) < DRAGGING_DELAY) {
                draggedPiece.baseX = destinationX;
                draggedPiece.baseY = destinationY;

                selectPiece(flipped ? 8 - file : file, flipped ? 9 - rank : rank, currentTime);
            } else {

                if (rank >= 0 && rank <= 9 && file >= 0 && file <= 8) {
                    movePiece(draggedPiece.file, draggedPiece.rank, flipped ? 8 - file : file, flipped ? 9 - rank : rank);
                    //return;
                    //draggedPiece.file = file;
                    //draggedPiece.rank = rank;
                } else {
                    destinationX = getXFromFile(draggedPiece.file);
                    destinationY = getYFromRank(draggedPiece.rank);
                }

                draggedPiece.baseX += draggedPiece.draggedX;
                draggedPiece.baseY += draggedPiece.draggedY;

                draggedPiece.animations.add(new Animation.MoveAnimation(destinationX
                        - draggedPiece.baseX,
                        destinationY - draggedPiece.baseY, draggedPiece.baseX,
                        draggedPiece.baseY, currentTime, 200));
                draggedPiece.animations.add(new Animation.ScaleAnimation(
                        1 - draggedPiece.scale, draggedPiece.scale,
                        currentTime, 200));
                draggedPiece.animations.add(new Animation.AlphaAnimation(55, 200,
                        currentTime, 200));
            }

            draggedPiece.draggedX = 0;
            draggedPiece.draggedY = 0;
        }
    }

    // -------------------------------------------------------------------------------------------------------

    private Bitmap loadScaledBitmap(Resources resources, int resourceId, float scaleX, float scaleY) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;
        Bitmap bitmap = BitmapFactory.decodeResource(resources, resourceId,
                options);
        return Bitmap.createScaledBitmap(bitmap,
                (int) (bitmap.getWidth() * scaleX),
                (int) (bitmap.getHeight() * scaleY), true);
    }

    // -------------------------------------------------------------------------------------------------------

    public void movePiece(int fromFile, int fromRank, int toFile, int toRank) {
        Log.d(TAG, "movePiece " + fromFile + " , " + fromRank + ", " + toFile + ", " + toRank);
        if (fromFile == toFile && fromRank == toRank) {
            Log.d(TAG, "Not moving!");
            initSprites();
        } else {
            Sprite.ChessPieceSprite chessPiece = findChessPieceSprite(fromFile, fromRank);
            if (chessPiece != null) {
                float vectorX = getXFromFile(flipped ? 8 - toFile : toFile) - chessPiece.baseX;
                float vectorY = getYFromRank(flipped ? 9 - toRank : toRank) - chessPiece.baseY;
                chessPiece.animations.add(new Animation.MoveAnimation(vectorX, vectorY,
                        chessPiece.baseX, chessPiece.baseY, System.currentTimeMillis(),
                        400));
            }
            if (pieceMoved(fromFile, fromRank, toFile, toRank, System.currentTimeMillis() + 100)) {
                if (chessPiece != null) {
                    chessPiece.file = toFile;
                    chessPiece.rank = toRank;
                }
            } else {
                initSprites();
            }
        }
        if (thread != null) {
            synchronized (thread) {
                thread.notify();
            }
        }
    }

    // -------------------------------------------------------------------------------------------------------

    @SuppressLint("ClickableViewAccessibility")
    public boolean onTouchEvent(MotionEvent event) {
        synchronized (inputEvents) {
            inputEvents.add(MotionEvent.obtain(event));
        }

        whenShouldThreadPause = System.currentTimeMillis() + 1000;
        synchronized (thread) {
            thread.notify();
        }

        return true;
    }

    // -------------------------------------------------------------------------------------------------------

    private boolean pieceMoved(int fromFile, int fromRank, int toFile, int toRank, long secondaryAnimationTime) {
        int fromSq = Position.COORD_XY(fromFile + Position.FILE_LEFT, fromRank + Position.RANK_TOP);
        doSecondaryMoveAnimation(fromFile, fromRank, toFile, toRank, secondaryAnimationTime);
        Log.d(TAG, fromSq + " 1 pieceMoved fromFile " + fromFile + " fromRank " + fromRank + " toFile " + toFile + " toRank " + toRank);
        fromSq = (flipped ? Position.SQUARE_FLIP(fromSq) : fromSq);
        int toSq = Position.COORD_XY(toFile + Position.FILE_LEFT, toRank + Position.RANK_TOP);
        Log.d(TAG, toSq + " 2 pieceMoved fromFile " + fromFile + " fromRank " + fromRank + " toFile " + toFile + " toRank " + toRank);
        toSq = (flipped ? Position.SQUARE_FLIP(toSq) : toSq);
        Log.d(TAG, "Starting to move! " + fromSq + " to " + toSq);
        int mv = Position.MOVE(fromSq, toSq);
        if (thinking) {
            Log.d(TAG, "It's opponents move - I will not move!");
            sqSelected = 0;
            state.mvLast = 0;
            return false;
        }
        boolean success = state.makeMove(mv);
        if (!success) {
            Log.d(TAG, "makeMove returned false!");
            sqSelected = 0;
            return false;
        }
        Log.d(TAG, "MOVE MADE!");
        if (moveListener != null) {
            moveListener.pieceMoved();
        }

        sqSelected = 0;
        if (!state.isFinished(-1, false)) {
            startThinking();
        }

        return true;
    }

    // -------------------------------------------------------------------------------------------------------

    private void selectPiece(int file, int rank, long currentTime) {
        Log.d(TAG, "selectPiece " + file + ", " + rank);
        selectedPiece = findChessPieceSprite(file, rank);
        if (selectedPiece == null) {
            return;
        }
        int sq_ = Position.COORD_XY(file + Position.FILE_LEFT, rank + Position.RANK_TOP);
        Log.d(TAG, sq_ + " selectPiece " + file + ", " + rank);
        int sq = (flipped ? Position.SQUARE_FLIP(sq_) : sq_);
        int pc = state.pos.squares[sq];
        if ((pc & Position.SIDE_TAG(state.pos.sdPlayer)) != 0) {
            if (state.mvLast > 0) {
                state.mvLast = 0;
            }
            sqSelected = sq;
            Log.d(TAG, "selectPiece sqSelected " + sqSelected + " sq " + sq);

            selectedPiece.animations.clear();
            selectedPiece.baseX = getXFromFile(flipped ? 8 - file : file);
            selectedPiece.baseY = getYFromRank(flipped ? 9 - rank : rank);
            selectedPiece.alpha = 255;
            selectedPiece.scale = 1;

            // create sprite under selected piece
            sprites.remove(selectedPieceSprite);
            selectedPieceSprite.baseX = getXFromFile(flipped ? 8 - file : file);
            selectedPieceSprite.baseY = getYFromRank(flipped ? 9 - rank : rank);
            selectedPieceSprite.alpha = 0;
            selectedPieceSprite.animations.clear();
            selectedPieceSprite.animations.add(new Animation.AlphaAnimation(255, 0,
                    currentTime, 100));
            addSprite(selectedPieceSprite);
            Log.d(TAG, "Selected piece sprite added at " + selectedPiece.baseX + ", " + selectedPiece.baseY);

            // create sprites highlighting possible moves
            for (int x = Position.FILE_LEFT; x <= Position.FILE_RIGHT; x++) {
                for (int y = Position.RANK_TOP; y <= Position.RANK_BOTTOM; y++) {
                    Log.d(TAG, "1 Checking move  x = " + x + " y = " + y);
                    int sq1 = Position.COORD_XY(x, y);
                    sq1 = (flipped ? Position.SQUARE_FLIP(sq1) : sq1);
                    int file1 = x - Position.FILE_LEFT;
                    int rank1 = y - Position.RANK_TOP;
                    int mv = Position.MOVE(sqSelected, sq1);
                    if (state.pos.legalMove(mv)) {
                        Log.d(TAG, "3 Adding legal moves highlight " + PIECE_NAME[pc] + " to file " + file1 + " rank " + rank1);
                        Sprite legalSquareSprite = new Sprite.ChessPieceSprite(legalMoveHighlightBitmap, file1, rank1, 6, this);
                        legalSquareSprite.alpha = 255;
                        legalSquareSprite.animations.add(new Animation.AlphaAnimation(200, 0, currentTime, 100));
                        addSprite(legalSquareSprite);
                        legalSquareSprites.add(legalSquareSprite);
                    }
                }
            }
            pieceSelected = true;
        }
    }

    // -------------------------------------------------------------------------------------------------------

    public void setFlipped(boolean flipped) {
        this.flipped = flipped;
        initSprites();
        if (thread != null) {
            synchronized (thread) {
                thread.notify();
            }
        }
    }

    // -------------------------------------------------------------------------------------------------------


    public void setSpriteZOrder(Sprite sprite, int z) {
        sprites.remove(sprite);
        sprite.z = z;
        addSprite(sprite);
    }

    // -------------------------------------------------------------------------------------------------------

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
    }

    // -------------------------------------------------------------------------------------------------------

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        thread.sendStopRequest();

        synchronized (thread) {
            thread.notify();
        }

        boolean retry = true;
        while (retry) {
            try {
                thread.join();
                thread = null;
                retry = false;
            } catch (InterruptedException ignored) {
            }
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        float scaleX = (float) width / CHESSBOARD_WIDTH;
        float scaleY = (float) height / CHESSBOARD_HEIGHT;
        squareSizeX = width / 9f;
        squareSizeY = (height - 25) / 10f;

        // load bitmaps
        Resources resources = getResources();
        imgBoard = loadScaledBitmap(resources, R.drawable.board, scaleX, scaleY);
        for (int i = 0; i < PIECE_NAME.length; i++) {
            if (PIECE_NAME[i] != null) {
                int productImageId = resources.getIdentifier(
                        PIECE_NAME[i], "drawable", getContext().getPackageName());
                imgPieces[i] = loadScaledBitmap(resources, productImageId, scaleX / 3, scaleY / 3);
            }
        }

        lastMoveHighlightBitmap = loadScaledBitmap(resources, R.drawable.square_frame, scaleX * 2, scaleY * 2);
        legalMoveHighlightBitmap = loadScaledBitmap(resources, R.drawable.legal_move_highlight, scaleX * 3, scaleY * 3);
        selectedPieceBitmap = loadScaledBitmap(resources, R.drawable.selected_piece, scaleX * 3, scaleY * 3);

        initSprites();

        thread = new DrawingThread();
        thread.start();
    }

    // -------------------------------------------------------------------------------------------------------

    public void undoMove() {
        if (!thinking) {
            state.currentFen = state.retractFen;
            restart();
            initSprites();
            if (thread != null) {
                synchronized (thread) {
                    thread.notify();
                }
            }
        }
    }

    public void start(String fen) {
        state.currentFen = fen;
        restart();
    }

    // -------------------------------------------------------------------------------------------------------

    private class DrawingThread extends Thread {

        private boolean stopRequest = false;

        @Override
        public void run() {
            while (!stopRequest) {
                // process input events
                synchronized (inputEvents) {
                    long currentTime = System.currentTimeMillis();
                    for (int i = 0; i < inputEvents.size(); i++) {
                        MotionEvent event = inputEvents.get(i);
                        if (event.getAction() == MotionEvent.ACTION_DOWN) {
                            inputDown(event.getX(), event.getY(), currentTime);
                        } else if (event.getAction() == MotionEvent.ACTION_MOVE) {
                            inputMove(event, currentTime);
                        } else if (event.getAction() == MotionEvent.ACTION_UP) {
                            inputUp(event, currentTime);
                        }
                        event.recycle();
                    }
                    inputEvents.clear();
                }

                boolean noOngoingAnimations = true;
                Canvas canvas = null;
                try {
                    canvas = surfaceHolder.lockCanvas();
                    long time = System.currentTimeMillis();
                    for (int i = sprites.size() - 1; i >= 0; i--) {
                        Sprite sprite = sprites.get(i);
                        sprite.draw(canvas, time);
                        if (sprite.animations.size() > 0)
                            noOngoingAnimations = false;
                    }
                } catch (Exception x) {
                    //Log.d(TAG, "Exception in the drawing thread: " + x.getLocalizedMessage());
                } finally {
                    if (canvas != null)
                        surfaceHolder.unlockCanvasAndPost(canvas);
                }

                if (noOngoingAnimations
                        && System.currentTimeMillis() > whenShouldThreadPause) {
                    synchronized (this) {
                        try {
                            this.wait();
                        } catch (InterruptedException ignored) {
                        }
                    }
                }
            }
        }

        public void sendStopRequest() {
            stopRequest = true;
        }

    }

    // -------------------------------------------------------------------------------------------------------

    private class RemoveAnimation extends Animation {

        public RemoveAnimation(long startTime, long length) {
            super(startTime, length);
        }

        public void update(Sprite sprite, long time) {
            if (time > startTime + length) {
                sprites.remove(sprite);
            }
        }
    }

    void restart() {
        state.restart();
        sqSelected = (state.mvLast = 0);

        Log.d(TAG, "restart sdPlayer = " + state.pos.sdPlayer + " onTurnPlayerId = " + state.onTurnPlayerId);
        startThinking();
    }

    // This method will start making move only when the computer should make move
    private void startThinking() {
        Log.d(TAG, "startThinking ");
        if (state.canMoveComputer(whiteComputer, blackComputer)) {
            thinking();
        } else {
            Log.d(TAG, "startThinking did nothing.");
        }
    }

    private void thinking() {
        thinking = true;
        new Thread(() -> {
            Log.d(TAG, "Will search for millis: " + level * 1000);
            int response = state.makeAutomaticMove(level);

            int fromFile = Position.FILE_X(Position.SRC(state.mvLast)) - Position.FILE_LEFT;
            int fromRank = Position.RANK_Y(Position.SRC(state.mvLast)) - Position.RANK_TOP;
            int toFile = Position.FILE_X(Position.DST(state.mvLast)) - Position.FILE_LEFT;
            int toRank = Position.RANK_Y(Position.DST(state.mvLast)) - Position.RANK_TOP;

            if ((imgBoard != null) && (lastMoveHighlights[0] != null)) {
                movePiece(fromFile, fromRank, toFile, toRank);
                lastMoveHighlights[0].baseX = getXFromFile(fromFile);
                lastMoveHighlights[0].baseY = getYFromRank(fromRank);
                lastMoveHighlights[0].animations.add(new Animation.AlphaAnimation(255, 0,
                        System.currentTimeMillis(), 100));
                lastMoveHighlights[1].baseX = getXFromFile(toFile);
                lastMoveHighlights[1].baseY = getYFromRank(toRank);
                lastMoveHighlights[1].animations.add(new Animation.AlphaAnimation(255, 0,
                        System.currentTimeMillis(), 100));

            }
            state.isFinished(response, false);
            thinking = false;
            if (moveListener != null) {
                moveListener.pieceMoved();
            }
        }
        ).start();
    }

    /**
     * Player Move Result
     */
    boolean isFinished() {
        return state.isFinished(-1, true);
    }

    public interface MoveListener {
        void pieceMoved();
    }

    public String getGameInfo1() {
        if (isFinished()) {
            return getResources().getString(R.string.finished);
        } else {
            if (state.pos.sdPlayer == 0) {
                return getResources().getString(R.string.redOnMove);
            } else {
                return getResources().getString(R.string.blackOnMove);
            }
        }
    }

    public String getGameInfo2() {
        switch (state.status) {
            case GameState.RESP_WIN:
                return getResources().getString(R.string.youWin);
            case GameState.RESP_LOSS:
                return getResources().getString(R.string.youLoose);
            default:
                if (state.doNotMoveHuman(whiteComputer, blackComputer, whiteRemote, blackRemote)) {
                    return getResources().getString(R.string.wait);
                } else {
                    return getResources().getString(R.string.play);
                }
        }
    }
}