package cz.solutions.chch.remote;

import android.app.Activity;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.games.GamesSignInClient;
import com.google.android.gms.games.PlayGames;
import com.google.android.gms.games.Player;

import java.util.Objects;

import cz.solutions.chch.GameState;
import cz.solutions.chch.dto.GameTableDTO;
import cz.solutions.chch.dto.PlayerDTO;

public class ServerConnectionService extends Service {
    public static final String TAG = "ServerConnectionService";

    public GameState mTurnData;
    String mPlayerId;
    String mPlayerImageUri;
    String mPlayerDisplayName;

    ServerConnection serverConnection;
    private GameTableDTO myTable;

    public ServerConnectionService() {
        serverConnection = new ServerConnection(this);
    }
    // Binder given to clients
    private final IBinder binder = new LocalBinder();

    /**
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    public class LocalBinder extends Binder {
        public ServerConnectionService getService() {
            // Return this instance of LocalService so clients can call public methods
            return ServerConnectionService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    public void signInGoogle(Activity a) {
        if (mPlayerId != null) {
            Log.d(TAG, "Already signed with id " + mPlayerId);
            return;
        }
        GamesSignInClient gamesSignInClient = PlayGames.getGamesSignInClient(a);

        gamesSignInClient.isAuthenticated().addOnFailureListener(failedTask -> {
            Log.e(TAG, "Sign in failed with " + failedTask.getMessage());
        }).addOnCompleteListener(isAuthenticatedTask -> {
            boolean isAuthenticated =
                    (isAuthenticatedTask.isSuccessful() &&
                            isAuthenticatedTask.getResult().isAuthenticated());

            if (isAuthenticated) {
                onConnected(a);
            } else {
                Log.d(TAG, "Client is not authenticated! isSuccessful == " + isAuthenticatedTask.isSuccessful());
                // Disable your integration with Play Games Services or show a
                // login button to ask  players to sign-in. Clicking it should
                gamesSignInClient.signIn().addOnFailureListener(failedTask -> {
                    Log.e(TAG, "gamesSignInClient.signIn() in failed with " + failedTask.getMessage());
                }).addOnCompleteListener(signInTask -> {
                    Log.d(TAG, "gamesSignInClient.signIn() completed " + signInTask.getResult().isAuthenticated());
                });
            }
        }).addOnCanceledListener(() -> {
            Log.e(TAG, "Sign in canceled");
        });
    }
    private void onConnected(Activity a) {
        if (serverConnection.isRunning()) {
            Log.d(TAG, "onConnected: already running!");
            return;
        }
        Log.d(TAG, "onConnected: connected to Google APIs");
        PlayGames.getPlayersClient(a).getCurrentPlayer().addOnCompleteListener(mTask -> {
                    Player player = mTask.getResult();
                    mPlayerId = player.getPlayerId();
                    if (player.hasHiResImage()) {
                        mPlayerImageUri = Objects.requireNonNull(player.getHiResImageUri()).toString();
                    } else if (player.hasIconImage()) {
                        mPlayerImageUri = Objects.requireNonNull(player.getIconImageUri()).toString();
                    }
                    Log.d(TAG, "onConnected: Connection successful mPlayerId=" + mPlayerId + " photoURL=" + mPlayerImageUri);

                    mPlayerDisplayName = player.getDisplayName();
                    Log.d(TAG, "onConnected: mPlayerDisplayName " + mPlayerDisplayName);
//        ImageView iv = findViewById(R.id.playerImage);
//        Picasso.get().load(mPlayerImageUri).into(iv);
                    serverConnection.start();
                }
        );
    }

    static void runOnMainUIThread(Runnable r) {
        new Handler(Looper.getMainLooper()).post(r);
    }

    public GameTableDTO getMyTable() {
        return myTable;
    }

    public void setMyTable(GameTableDTO t) {
        Log.d(TAG, "setMyTable " + t);
        if (t != null && findPlayerAtTable(mPlayerId, t) != null) {
            if (myTable != null) {
                t.setState(myTable.getState());
            }
            myTable = t;
            refreshGameStateFromTable();
        } else {
            myTable = null;
        }
        //setViewVisibility();
    }
    private void refreshGameStateFromTable() {
        String state = myTable.getState();
        Log.d(TAG, "refreshGameStateFromTable loading state " + state);
        if (state != null && state.length()>0) {
            mTurnData = GameStateSerializer.load(state);
            // TODO: add a check whether both black and white are still present
        } else {
            mTurnData = null;
        }
//        mGameView.setState(mTurnData);
//        setViewVisibility();
    }
    private PlayerDTO findPlayerAtTable(String playerId, GameTableDTO table) {
        //Log.d(TAG, "findPlayerAtTable " + playerId + " table: " + table);
        if (table == null) {
            //throw new IllegalArgumentException("Parameter table cannot be null");
            return null;
        }
        for (PlayerDTO p : table.getPlayers()) {
            if (p.getUid().equals(playerId)) {
                return p;
            }
        }
        return null;
    }
    String getPlayerImageURL(String playerId) {
        PlayerDTO playerAtMyTable = findPlayerAtTable(playerId, myTable);
        if (playerAtMyTable != null) {
            return playerAtMyTable.getPhotoURL();
        }
        return null;
    }

    private void onDisconnected() {
        Log.d(TAG, "onDisconnected()");
        mPlayerId = null;
        mPlayerImageUri = null;
        mPlayerDisplayName = null;
        serverConnection.stop();
//        setViewVisibility();
    }
    private Looper serviceLooper;
    private ServiceHandler serviceHandler;

    // Handler that receives messages from the thread
    private final class ServiceHandler extends Handler {
        public ServiceHandler(Looper looper) {
            super(looper);
        }
        @Override
        public void handleMessage(Message msg) {
            // Normally we would do some work here, like download a file.
            Log.d(TAG, "handleMessage " + msg);
            stopSelf(msg.arg1);
        }
    }

    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate");
        // Start up the thread running the service. Note that we create a
        // separate thread because the service normally runs in the process's
        // main thread, which we don't want to block. We also make it
        // background priority so CPU-intensive work doesn't disrupt our UI.
        HandlerThread thread = new HandlerThread("ServiceStartArguments",
                Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();

        // Get the HandlerThread's Looper and use it for our Handler
        serviceLooper = thread.getLooper();
        serviceHandler = new ServiceHandler(serviceLooper);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand" );
        Toast.makeText(this, "service starting", Toast.LENGTH_SHORT).show();

        // For each start request, send a message to start a job and deliver the
        // start ID so we know which request we're stopping when we finish the job
        Message msg = serviceHandler.obtainMessage();
        msg.arg1 = startId;
        serviceHandler.sendMessage(msg);

        // If we get killed, after returning from here, restart
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy");
        Toast.makeText(this, "service done", Toast.LENGTH_SHORT).show();
    }

}