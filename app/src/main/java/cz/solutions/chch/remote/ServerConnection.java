package cz.solutions.chch.remote;

import static com.google.common.util.concurrent.Futures.addCallback;
import static com.google.common.util.concurrent.Futures.immediateFuture;
import static com.google.common.util.concurrent.Futures.transform;
import static com.google.common.util.concurrent.Futures.transformAsync;

import android.content.SharedPreferences;
import android.os.Looper;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.preference.PreferenceManager;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.firebase.crashlytics.buildtools.reloc.org.checkerframework.checker.nullness.compatqual.NullableDecl;
import com.heroiclabs.nakama.AbstractSocketListener;
import com.heroiclabs.nakama.Client;
import com.heroiclabs.nakama.DefaultClient;
import com.heroiclabs.nakama.DefaultSession;
import com.heroiclabs.nakama.Match;
import com.heroiclabs.nakama.MatchData;
import com.heroiclabs.nakama.MatchPresenceEvent;
import com.heroiclabs.nakama.MatchmakerMatched;
import com.heroiclabs.nakama.MatchmakerTicket;
import com.heroiclabs.nakama.Session;
import com.heroiclabs.nakama.SocketClient;
import com.heroiclabs.nakama.StatusPresenceEvent;
import com.heroiclabs.nakama.UserPresence;
import com.heroiclabs.nakama.api.User;
import com.heroiclabs.nakama.api.Users;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import cz.solutions.chch.R;
import cz.solutions.chch.dto.GameTableDTO;
import cz.solutions.chch.dto.PlayerDTO;

class ServerConnection {
    private static final String TAG = "ServerConnection";
    private final ServerConnectionService myService;
    private boolean running;
    private static final ExecutorService executorService = Executors.newCachedThreadPool();

    private Client client;
    private Session session;
    private SocketClient socket;
    private Match match;
    private MatchmakerTicket matchmakerTicket;
    private final SocketListener listener = new SocketListener();

    ServerConnection(ServerConnectionService ms) {
        this.myService = ms;

    }

    boolean isRunning() {
        return running;
    }

    void start() {
        if (!Looper.getMainLooper().isCurrentThread()) {
            throw new IllegalStateException("Start must be called from the main thread.");
        }
        this.client = new DefaultClient(myService.getResources().getString(R.string.nakamaServerSecret), "chch.solutions.cz", 7149, true) {
            @Override
            public SocketClient createSocket() {
                return super.createSocket(7150);
            }
        };
        if (running) {
            return;
        }
        running = true;
        Log.d(TAG, "starting nakama client");
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(myService.getApplicationContext());
        // Lets check if we can restore a cached session.
        String sessionString = pref.getString("nk.session", null);
        String refreshString = pref.getString("nk.refresh", null);
        if (sessionString != null && !sessionString.isEmpty() && refreshString != null && !refreshString.isEmpty()) {
            Session restoredSession = DefaultSession.restore(sessionString, refreshString);
            if (!restoredSession.isExpired(new Date())) {
                Log.d(TAG, "restored session " + restoredSession);
                socket = client.createSocket();
                rememberSessionAndConnect(restoredSession);
                return;
            }
        }

        socket = client.createSocket();

        initSession();
    }

    private void initSession() {
        Log.d(TAG, "initSession() with id " + myService.mPlayerId);
        ListenableFuture<Session> sessionFuture = client.authenticateCustom(myService.mPlayerId, true);
        addCallback(sessionFuture, new FutureCallback<Session>() {
            @Override
            public void onSuccess(@NullableDecl Session ses) {
                Log.d(TAG, "new session " + ses);
                if (ses != null) {
                    rememberSessionAndConnect(ses);
                }
            }

            @Override
            public void onFailure(@NonNull Throwable t) {
                Log.d(TAG,"getting session failed", t);
            }
        }, executorService);
    }

    private void rememberSessionAndConnect(@NonNull Session ses) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(myService.getApplicationContext());
        pref.edit().putString("nk.session", ses.getAuthToken()).apply();
        pref.edit().putString("nk.refresh", ses.getRefreshToken()).apply();
        session = ses;
        client.updateAccount(session, myService.mPlayerId, myService.mPlayerDisplayName, myService.mPlayerImageUri);
        socket.connect(session, listener);
    }

    private ListenableFuture<Void> initSessionAsync() {
        ListenableFuture<Session> sessionFuture = client.authenticateCustom(myService.mPlayerId);
        return transformAsync(sessionFuture, ses -> {
            Log.d(TAG, "new session async " + ses);
            if (ses != null) {
                rememberSessionAndConnect(ses);
            }
            return immediateFuture(null);
        }, executorService);
    }

    private void postCreateTable() {
        FutureCallback<GameTableDTO> futureCallback = new FutureCallback<GameTableDTO>() {
            @Override
            public void onSuccess(@NullableDecl GameTableDTO table) {
              Log.d(TAG, "onSuccess table " + table);
                ServerConnectionService.runOnMainUIThread(() -> myService.setMyTable(table));
            }

            @Override
            public void onFailure(@NonNull Throwable t) {
              Log.d(TAG, "createTableFromMatch failed", t);
            }
        };
        if (session.isExpired(new Date())) {
            ListenableFuture<GameTableDTO> gameTableDTOListenableFuture =
                    transformAsync(initSessionAsync(), v -> createTableFromMatch(), executorService);
            addCallback(gameTableDTOListenableFuture, futureCallback, executorService);
        } else {
            addCallback(createTableFromMatch(), futureCallback, executorService);
        }
    }

    private ListenableFuture<GameTableDTO> createTableFromMatch() {
        if (match == null) {
            throw new IllegalStateException("Cannot create table if match == null");
        }
        List<String> ids = new ArrayList<>();
        List<String> userNames = new ArrayList<>();

        for (UserPresence opponent : match.getPresences()) {
            ids.add(opponent.getUserId());
            userNames.add(opponent.getUsername());
        }
        //Log.d(TAG, "Will call getUsers with params session= " + session + " ids= " + ids + " usernames= " + userNames);
      //Log.d(TAG, "Will call getUsers with usernames= " + userNames);
        ListenableFuture<Users> usersFuture = client.getUsers(session, ids, userNames);
        return transform(usersFuture, (users) -> {
            GameTableDTO gt = new GameTableDTO();
            gt.setId(new Random().nextLong());
            for (User u : users.getUsersList()) {
                PlayerDTO p = new PlayerDTO(u.getId().hashCode(), u.getDisplayName(), u.getGoogleId(),
                        u.getId(), u.getAvatarUrl(), 0);
                gt.addPlayer(p);
            }
            Log.d(TAG, "Just created table " + gt);
            return gt;
        }, executorService);
    }

    void stop() {
        //Log.d(TAG, "stop() called.");
                // new RuntimeException("Please ignore the exception - just logging from where the stop was called."));
        running = false;
        //socket.disconnect();
    }

    void createNewMatchMaker() {
        if (socket == null) {
            Log.d(TAG, "Cannot create matchmaker when not connected");
            return;
        }
        ListenableFuture<MatchmakerTicket> ticketFuture = socket.addMatchmaker(2, 2);
        addCallback(ticketFuture, new FutureCallback<MatchmakerTicket>() {
            @Override
            public void onSuccess(@NullableDecl MatchmakerTicket mt) {
                matchmakerTicket = mt;
                Log.d(TAG, "Got matchmakerTicket from server " + matchmakerTicket);
                ServerConnectionService.runOnMainUIThread(()->{
                    //ma.setViewVisibility();
                });
            }

            @Override
            public void onFailure(@NonNull Throwable t) {
                Log.d(TAG,"socket.addMatchmaker failed", t);
            }
        }, executorService);

    }

    boolean isInMatch() {
        return matchmakerTicket != null || match != null;
    }

    void saveTableState() {
        Log.d(TAG, "saveTableState start");
        GameTableDTO table = myService.getMyTable();
        if (table == null) {
            throw new IllegalStateException("saveTableState table == null");
        }
        if (match == null) {
            throw new IllegalStateException("saveTableState match == null");
        }
        String state = table.getState();
        Log.d(TAG, "saveTableState sending state " + state);
        if (state == null) {
            state = "";
        }
        socket.sendMatchData(match.getMatchId(), 1, state.getBytes());
    }

    void leaveMatch() {
        Log.d(TAG, "leaveMatch start");
        if (matchmakerTicket != null) {
            ListenableFuture<Void> listenableFuture = socket.removeMatchmaker(matchmakerTicket.getTicket());
            addCallback(listenableFuture, new FutureCallback<Void>() {
                @Override
                public void onSuccess(@NullableDecl Void aVoid) {
                    Log.d(TAG, "Dropping matchmakerTicket " + matchmakerTicket);
                    matchmakerTicket = null;
                    Log.d(TAG, "matchmakerTicket = null");
                    ServerConnectionService.runOnMainUIThread(()-> myService.setMyTable(null));
                }

                @Override
                public void onFailure(@NonNull Throwable t) {
                    Log.d(TAG,"removeMatchmaker failed", t);
                }
            }, executorService);

        }
        if (match != null) {
            ListenableFuture<Void> listenableFuture = socket.leaveMatch(match.getMatchId());
            addCallback(listenableFuture, new FutureCallback<Void>() {
                @Override
                public void onSuccess(@NullableDecl Void aVoid) {
                    Log.d(TAG, "Dropping match " + match);
                    match = null;
                    Log.d(TAG, "match = null");
                    ServerConnectionService.runOnMainUIThread(()-> myService.setMyTable(null));
                }

                @Override
                public void onFailure(@NonNull Throwable t) {
                    Log.d(TAG,"leaveMatch failed", t);
                }
            }, executorService);

        }
    }

    private class SocketListener extends AbstractSocketListener {
        @Override
        public void onDisconnect(final Throwable t) {
            Log.d(TAG, "Socket disconnected", t);
        }

        @Override
        public void onStatusPresence(final StatusPresenceEvent presence) {
            Log.d(TAG, "onStatusPresence " + presence);
            for (UserPresence userPresence : presence.getJoins()) {
                Log.d(TAG, "User ID: " + userPresence.getUserId() + " Username: " + userPresence.getUsername() + " Status: " + userPresence.getStatus());
            }

            for (UserPresence userPresence : presence.getLeaves()) {
                Log.d(TAG,  "User ID: " + userPresence.getUserId() + " Username: " + userPresence.getUsername() + " Status: " + userPresence.getStatus());
            }
        }

        @Override
        public void onMatchmakerMatched(final MatchmakerMatched matched) {
            if (Looper.getMainLooper().isCurrentThread()) {
                throw new IllegalStateException("onMatchmakerMatched must not be called from the main thread.");
            }
            Log.d(TAG, "onMatchmakerMatched " + matched);
            try {
                match = socket.joinMatchToken(matched.getToken()).get();
                Log.d(TAG, "Server gave me match " + match);
                Log.d(TAG, "As I have match, I am dropping matchmaker ticket " + matchmakerTicket);
                matchmakerTicket = null;
                ServerConnectionService.runOnMainUIThread(ServerConnection.this::postCreateTable);
            } catch (Exception ex) {
                Log.d(TAG,"socket.joinMatchToken failed", ex);
            }
        }

        @Override
        public void onMatchPresence(final MatchPresenceEvent matchPresence) {
            Log.d(TAG,"Received MatchPresenceEvent " + matchPresence);
            if (match == null) {
                return;
            }
            List<UserPresence> presences = match.getPresences();
            if (matchPresence.getJoins() != null) {
                for (UserPresence p : matchPresence.getJoins()) {
                    if (!presences.contains(p)) {
                        presences.add(p);
                    }
                }
            }
            if (matchPresence.getLeaves() != null) {
                for (UserPresence p : matchPresence.getLeaves()) {
                    presences.remove(p);
                }
            }
            Log.d(TAG, "Opponents after update from the event: " + match.getPresences());
            String matchId = matchPresence.getMatchId();
            if (matchId.equals(match.getMatchId())) {
                ServerConnectionService.runOnMainUIThread(ServerConnection.this::postCreateTable);
            } else {
                Log.d(TAG, "Got presence with a different match id " + matchId + " my original was " + match.getMatchId());
            }
        }

        @Override
        public void onMatchData(@NonNull final MatchData matchData) {
            Log.d(TAG,"Received match data with opcode " + matchData.getOpCode());
            final GameTableDTO t = myService.getMyTable();
            if (t != null) {
                t.setState(new String(matchData.getData()));
                ServerConnectionService.runOnMainUIThread(() -> myService.setMyTable(t));
            }
        }
    }

}
