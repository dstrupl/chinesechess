package cz.solutions.chch.remote;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;

import cz.solutions.chch.GameState;

public class GameStateSerializer {
    private static final String ENCODING = "UTF-8";

    /**
     * Deserializes the GameState.
     * @param base64Encoded json that was saved by the method save
     * @return new GameState instance filled in from the parameter
     */
    public static GameState load(String base64Encoded) {
        if (base64Encoded == null) {
            throw new IllegalArgumentException("The loaded string must not be null");
        }

        byte[] byteArray = java.util.Base64.getDecoder().decode(base64Encoded);

        if (byteArray.length > 128 * 1024) {
            throw new IllegalArgumentException("The loaded array must be shorter than 128K.");
        }

        String st;
        try {
            st = new String(byteArray, ENCODING);
        } catch (UnsupportedEncodingException e1) {
            throw new IllegalArgumentException("Problem converting byte array to UTF-8 string ", e1);
        }

        Moshi moshi = new Moshi.Builder().build();
        JsonAdapter<GameState> jsonAdapter = moshi.adapter(GameState.class);

        try {
            return jsonAdapter.fromJson(st);
        } catch (IOException e) {
            throw new IllegalArgumentException("Error while parsing JSON " + st, e);
        }
    }

    /**
     * Serializes the GameState.
     * @param state
     * @return base64 encoded json that the method load can transform back
     */
    public static String save(GameState state) {
        String json = toJson(state);
        //Log.d(TAG, "==== Saving json\n" + json);
        byte[] resultArray = json.getBytes(Charset.forName(ENCODING));
        if (resultArray.length > 128 * 1024) {
            throw new IllegalStateException("Attempting to save more than 128K is not allowed. Size = " + resultArray.length);
        }
        String result = java.util.Base64.getEncoder().encodeToString(resultArray);
        return result;
    }

    static String toJson(GameState state) {
        Moshi moshi = new Moshi.Builder().build();
        JsonAdapter<GameState> jsonAdapter = moshi.adapter(GameState.class);
        return jsonAdapter.toJson(state);
    }
}
