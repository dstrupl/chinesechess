/*
 * Copyright (C) 2012 Ciaran Gultnieks, ciaran@ciarang.com
 * Copyright (C) 2010 František Hejl
 * Copyright (C) 2019 David Strupl
 *
 * This file is part of Chinese Chess.
 *
 * Chinese Chess is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Chesswalk is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Chinese Chess.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package cz.solutions.chch;

abstract class Animation {

    public boolean finished = false;

    protected final long startTime;
    protected final long length;

    public Animation(long startTime, long length) {
        this.length = length;
        this.startTime = startTime;
    }

    public abstract void update(Sprite sprite, long time);

    static class MoveAnimation extends Animation {

        private final float baseX;
        private final float baseY;
        private final float vectorX;
        private final float vectorY;

        public MoveAnimation(float vectorX, float vectorY, float baseX,
                             float baseY, long startTime, long length) {
            super(startTime, length);
            this.vectorX = vectorX;
            this.vectorY = vectorY;
            this.baseX = baseX;
            this.baseY = baseY;
        }

        public void update(Sprite sprite, long time) {
            float progress;
            if (time < startTime + length) {
                if (time - startTime < 0)
                    progress = 0.0f;
                else
                    progress = (time - startTime) / (float) length;
            } else {
                progress = 1.0f;
                finished = true;
            }

            sprite.baseX = vectorX * progress + baseX;
            sprite.baseY = vectorY * progress + baseY;
        }
    }

    static class AlphaAnimation extends Animation {

		private final int alphaChange;
		private final int baseAlpha;

		public AlphaAnimation(int alphaChange, int baseAlpha, long startTime,
							  long length) {
			super(startTime, length);
			this.alphaChange = alphaChange;
			this.baseAlpha = baseAlpha;
		}

		public void update(Sprite sprite, long time) {
			float progress;
			if (time < startTime + length) {
				if (time - startTime < 0)
					progress = 0.0f;
				else
					progress = (time - startTime) / (float) length;
			} else {
				progress = 1.0f;
				finished = true;
			}

			sprite.alpha = (int) (baseAlpha + alphaChange * progress);
		}
	}

    static class ScaleAnimation extends Animation {

        private final float baseScale;
        private final float scaleChange;

        public ScaleAnimation(float scaleChange, float baseScale,
                              long startTime, long length) {
            super(startTime, length);
            this.baseScale = baseScale;
            this.scaleChange = scaleChange;
        }

        public void update(Sprite sprite, long time) {
            float progress;
            if (time < startTime + length) {
                if (time - startTime < 0)
                    progress = 0.0f;
                else
                    progress = (time - startTime) / (float) length;
            } else {
                progress = 1.0f;
                finished = true;
            }

            sprite.scale = baseScale + scaleChange * progress;
        }
    }
}
