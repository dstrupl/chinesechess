package cz.solutions.chch;

import android.util.Log;

import java.util.*;

import cz.solutions.chch.remote.GameStateSerializer;

public class GameState {
    private static final String TAG = "GameState";
    private String blackId;
    private String whiteId;
    String onTurnPlayerId;
    private String winnerId;
    String currentFen, retractFen;
    int mvLast;

    final transient Position pos = new Position();
    final transient Search search = new Search(pos, 16);

    int status = RESP_CLICK;
    static final int RESP_CLICK = 0;
    //	 static final int RESP_ILLEGAL = 1;
//	 static final int RESP_MOVE = 2;
    static final int RESP_MOVE2 = 3;
    //	 static final int RESP_CAPTURE = 4;
    static final int RESP_CAPTURE2 = 5;
    //	 static final int RESP_CHECK = 6;
    static final int RESP_CHECK2 = 7;
    static final int RESP_WIN = 8;
    static final int RESP_DRAW = 9;
    static final int RESP_LOSS = 10;

    public GameState() {
    }

    public void addPlayer(String playerId) {
        if (whiteId == null) {
            whiteId = playerId;
            return;
        }
        if (blackId == null) {
            blackId = playerId;
            return;
        }
        throw new IllegalStateException("Cannot have more than 2 players");
    }

    public void removePlayer(String playerId) {
        if (Objects.equals(whiteId, playerId)) {
            whiteId = null;
        }
        if (Objects.equals(blackId, playerId)) {
            blackId = null;
        }
        if (Objects.equals(winnerId, playerId)) {
            winnerId = null;
        }
    }

    @Override
    public String toString() {
        return "currentFen=" + currentFen + " retractFen=" + retractFen + " mvLast=" +
                mvLast + " onTurnPlayerId=" +
                onTurnPlayerId + " winnerId=" + winnerId;
    }

    public void init(String onTurnPlayerId) {
        this.onTurnPlayerId = onTurnPlayerId;
        winnerId = null;
    }

    private String getNextPlayerId() {
        if (Objects.equals(whiteId, onTurnPlayerId)) {
            return blackId;
        }
        if (Objects.equals(blackId, onTurnPlayerId)) {
            return whiteId;
        }
        throw new IllegalStateException("On turn player is neither white nor black");
    }

//    public List<String> getPlayers() {
//        return players;
//    }

    public String getOnTurnPlayerId() {
        return onTurnPlayerId;
    }

    public void setOnTurnPlayerId(String onTurnPlayerId) {
        this.onTurnPlayerId = onTurnPlayerId;
    }

    public String getWinnerId() {
        return winnerId;
    }
    /** Computer Move Result */
    boolean isFinished(int response, boolean useClone) {
        Log.d(TAG, "isFinished("+response+","+useClone+")");
        Position p = pos;
        if (useClone) {
            p = new Position();
            p.fromFen(pos.toFen());
        }
        if (p.isMate()) {
            if (!useClone) {
                status = response < 0 ? RESP_WIN : RESP_LOSS;
            }
            Log.d(TAG, useClone + " isFinished isMate " + response);
            return true;
        }
        int vlRep = p.repStatus(3);
        if (vlRep > 0) {
            vlRep = (response < 0 ? p.repValue(vlRep) : -p.repValue(vlRep));
            Log.d(TAG, "isFinished vlRep" + vlRep);
            if (!useClone) {
                status = vlRep > Position.WIN_VALUE ? RESP_LOSS :
                        vlRep < -Position.WIN_VALUE ? RESP_WIN : RESP_DRAW;
            }
            Log.d(TAG, "isFinished returning true and status = " + status);
            return true;
        }
        if (pos.moveNum > 100) {
            if (!useClone) {
                status = RESP_DRAW;
            }
            return true;
        }
        if (response >= 0) {
            retractFen = currentFen;
            currentFen = p.toFen();
        }
        status = RESP_CLICK;
        Log.d(TAG, "isFinished returning false");
        return false;
    }

    int computeResponse(Position pos) {
        return pos.inCheck() ? RESP_CHECK2 :
                pos.captured() ? RESP_CAPTURE2 : RESP_MOVE2;
    }

    public int makeAutomaticMove(int level) {
        Log.d(TAG, "makeAutomaticMove start");
        try {
            mvLast = search.searchMain(level * 1000);
        } catch (Exception x) {
            //x.printStackTrace();
            return -1;
        }
        Log.d(TAG, "makeAutomaticMove will ask pos to make move " + mvLast);
        pos.makeMove(mvLast);
        int response = computeResponse(pos);
        if (pos.captured()) {
            pos.setIrrev();
        }
        Log.d(TAG, "makeAutomaticMove returning " + response);
        return response;
    }

    public boolean makeMove(int mv) {
        if (!pos.legalMove(mv)) {
            Log.d(TAG, "Not a legal move!");
            mvLast = 0;
            return false;
        }
        if (!pos.makeMove(mv)) {
            Log.d(TAG, "makeMove returning false!");
            mvLast = 0;
            return false;
        }

        if (pos.captured()) {
            pos.setIrrev();
        }
        mvLast = mv;
        Log.d(TAG, "makeMove returning true");
        return true;
    }

    boolean doNotMoveHuman(boolean whiteComputer, boolean blackComputer, boolean whiteRemote, boolean blackRemote) {
        Log.d(TAG, "doNotMoveHuman whiteComputer=" + whiteComputer + " blackComputer=" + blackComputer);
        Log.d(TAG, "v pos.sdPlayer=" + pos.sdPlayer);
        return ((pos.sdPlayer == 0) && (whiteComputer || whiteRemote) ) || ((pos.sdPlayer == 1) && (blackComputer||blackRemote));
    }

    boolean canMoveComputer(boolean whiteComputer, boolean blackComputer) {
        Log.d(TAG, "doNotMoveComputer whiteComputer=" + whiteComputer + " blackComputer=" + blackComputer);
        Log.d(TAG, "doNotMoveComputer pos.sdPlayer=" + pos.sdPlayer);
        return ((pos.sdPlayer == 0) && whiteComputer) || ((pos.sdPlayer == 1) && blackComputer);
    }

    void restart() {
        pos.fromFen(currentFen);
        retractFen = currentFen;
    }
}
