package cz.solutions.chch.dto;

import java.util.Objects;

public class PlayerDTO {

    private long id;
    private String name;
    private String uid;
    private String email;
    private String photoURL;
    private int score;

    public PlayerDTO() {
    }

    public PlayerDTO(long id, String name, String uid, String email, String photoURL, int score) {
        this.id = id;
        this.name = name;
        this.uid = uid;
        this.email = email;
        this.photoURL = photoURL;
        this.score = score;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhotoURL() {
        return photoURL;
    }

    public void setPhotoURL(String photoURL) {
        this.photoURL = photoURL;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "PlayerDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", uid='" + uid + '\'' +
                ", email='" + email + '\'' +
                ", photoURL='" + photoURL + '\'' +
                ", score='" + score + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PlayerDTO)) return false;
        PlayerDTO player = (PlayerDTO) o;
        return getId() == player.getId() &&
                Objects.equals(getName(), player.getName()) &&
                getUid().equals(player.getUid()) &&
                Objects.equals(getEmail(), player.getEmail()) &&
                Objects.equals(getPhotoURL(), player.getPhotoURL());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getUid(), getEmail(), getPhotoURL());
    }
}