package cz.solutions.chch.dto;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class GameTableDTO {
    private long id;

    private String myUid;

    private List<PlayerDTO> players;

    private String state;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<PlayerDTO> getPlayers() {
        if (players == null) {
            return Collections.emptyList();
        }
        return players;
    }

    public void setPlayers(List<PlayerDTO> players) {
        this.players = players;
    }

    public void addPlayer(PlayerDTO p) {
        if (players == null) {
            players = new ArrayList<>();
        }
        players.add(p);
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "GameTableDTO{" +
                "id=" + id +
                ", playerEntities=" + players +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GameTableDTO)) return false;
        GameTableDTO gameTable = (GameTableDTO) o;
        return getId() == gameTable.getId() &&
                Objects.equals(getPlayers(), gameTable.getPlayers());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getPlayers());
    }

    public String getMyUid() {
        return myUid;
    }

    public void setMyUid(String myUid) {
        this.myUid = myUid;
    }
}
