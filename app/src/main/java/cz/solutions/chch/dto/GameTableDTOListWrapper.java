package cz.solutions.chch.dto;

import java.util.List;

public class GameTableDTOListWrapper {
    private List<GameTableDTO> wrappedList;

    public GameTableDTOListWrapper() {
    }

    public GameTableDTOListWrapper(List<GameTableDTO> wrappedList) {
        this.wrappedList = wrappedList;
    }

    public List<GameTableDTO> getWrappedList() {
        return wrappedList;
    }

    public void setWrappedList(List<GameTableDTO> wrappedList) {
        this.wrappedList = wrappedList;
    }
}