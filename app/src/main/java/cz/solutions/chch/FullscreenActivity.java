/*
 * Copyright (C) 2012 Ciaran Gultnieks, ciaran@ciarang.com
 * Copyright (C) 2010 František Hejl
 * Copyright (C) 2019, 2022 David Strupl
 *
 * This file is part of Chinese Chess.
 *
 * Chinese Chess is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Chesswalk is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Chinese Chess.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package cz.solutions.chch;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.preference.PreferenceManager;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.RequestConfiguration;
import com.google.android.gms.games.PlayGamesSdk;

import java.util.Arrays;
import java.util.List;

import cz.solutions.chch.ChessBoardView.MoveListener;
import cz.solutions.chch.remote.ServerConnectionService;

public class FullscreenActivity extends Activity implements View.OnClickListener, MoveListener {
    public static final String TAG = "FullscreenActivity";
    private ProgressBar pbThinking;
    private TextView tvOfflineGameInfo;
    private TextView tvOfflineTopInfo;
    private ChessBoardView chessBoard;

    private boolean gameEnded = false;
//    public static final int PLAYER_HUMAN = 0;
    public static final int PLAYER_COMPUTER = 1;
    public static final int PLAYER_REMOTE = 2;
    private int black;
    private int white;

    ServerConnectionService mService;
    boolean mBound = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_fullscreen);

        List<String> testDeviceIds = Arrays.asList("CCE11AE6A00C295FDC11AA13C7E317FA" , "D2FC2357D57C8716664E45E9DD3FDC6E");

        RequestConfiguration configuration =
                new RequestConfiguration.Builder().setTestDeviceIds(testDeviceIds).build();
        MobileAds.setRequestConfiguration(configuration);

        MobileAds.initialize(this);

        AdView mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        PlayGamesSdk.initialize(this);

        chessBoard = findViewById(R.id.offlineChessBoard);
        chessBoard.moveListener = this;

        // set up buttons
        findViewById(R.id.btNewGame).setOnClickListener(this);
        findViewById(R.id.btUndo).setOnClickListener(this);
        findViewById(R.id.btFlip).setOnClickListener(this);

        tvOfflineGameInfo = findViewById(R.id.tvOfflineGameInfo);
        pbThinking = findViewById(R.id.pbThinking);
        tvOfflineTopInfo = findViewById(R.id.tvOfflineTopInfo);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = new Intent(this, ServerConnectionService.class);
        bindService(intent, connection, Context.BIND_AUTO_CREATE);
        Log.d(TAG, "Binding started.");
    }

    @Override
    protected void onStop() {
        super.onStop();
        unbindService(connection);
        mBound = false;
    }

    @Override
    protected void onPause() {
        super.onPause();
        // save chessboard, whiteHuman, blackHuman and flipped
        SharedPreferences settings = PreferenceManager
                .getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("FEN", chessBoard.state.pos.toFen());
        editor.putInt("white", white);
        editor.putInt("black", black);
        editor.putBoolean("flipped", chessBoard.getFlipped());
        editor.putBoolean("gameEnded", gameEnded);
        editor.apply();
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences settings = PreferenceManager
                .getDefaultSharedPreferences(this);
        white = settings.getInt("white", 0);
        black = settings.getInt("black", 1);

        chessBoard.flipped = settings.getBoolean("flipped", false);
        int strength = settings.getInt("strength", 1);
        int handicap = settings.getInt("handicap", 0);
        gameEnded = settings.getBoolean("gameEnded", false);
        chessBoard.whiteComputer = (white == PLAYER_COMPUTER);
        chessBoard.blackComputer = (black == PLAYER_COMPUTER);
        chessBoard.whiteRemote = (white == PLAYER_REMOTE);
        chessBoard.blackRemote = (black == PLAYER_REMOTE);
        chessBoard.level = strength;
        chessBoard.handicap = handicap;
        String FEN = settings.getString("FEN", Position.STARTUP_FEN[chessBoard.handicap]);
        Log.d(this.getClass().getName(),"onResume will restore " + FEN);
        chessBoard.start(FEN);
        setTopInfo();
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btNewGame) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.confirm);
            builder.setMessage(R.string.confirmNewGame);
            builder.setPositiveButton(R.string.yes, (dialog, which) -> {
                dialog.dismiss();
                Intent i = new Intent(FullscreenActivity.this, NewGameActivity.class);
                startActivityForResult(i, 0);
            });
            builder.setNegativeButton(R.string.no, (dialog, which) -> dialog.dismiss());
            AlertDialog alert = builder.create();
            alert.show();
        } else if (v.getId() == R.id.btUndo) {
            chessBoard.undoMove();
            setTopInfo();
        } else if (v.getId() == R.id.btFlip) {
            chessBoard.setFlipped(!chessBoard.getFlipped());
        }
    }

    private void setTopInfo() {
        pbThinking.setVisibility(View.GONE);
        tvOfflineTopInfo.setText(chessBoard.getGameInfo1());
        tvOfflineGameInfo.setText(chessBoard.getGameInfo2());
    }

    @Override
    public void pieceMoved() {
        runOnUiThread(this::setTopInfo);
    }
    /** Defines callbacks for service binding, passed to bindService() */
    private final ServiceConnection connection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            ServerConnectionService.LocalBinder binder = (ServerConnectionService.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
            Log.d(TAG, "Bound");
            if (white == PLAYER_REMOTE || black == PLAYER_REMOTE) {
                mService.signInGoogle(FullscreenActivity.this);
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
            Log.d(TAG, "Unbound");
        }
    };

}
