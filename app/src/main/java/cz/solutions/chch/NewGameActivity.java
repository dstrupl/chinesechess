/*
 * Copyright (C) 2012 Ciaran Gultnieks, ciaran@ciarang.com
 * Copyright (C) 2010 František Hejl
 * Copyright (C) 2019 David Strupl
 *
 * This file is part of Chinese Chess.
 *
 * Chinese Chess is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Chesswalk is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Chinese Chess.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package cz.solutions.chch;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.preference.PreferenceManager;

import java.text.MessageFormat;

public class NewGameActivity extends Activity implements View.OnClickListener {

    private int white;
    private int black;
    private Button btBlackComputer;
    private Button btBlackHuman;
    private Button btBlackRemote;
    private Button btWhiteComputer;
    private Button btWhiteHuman;
    private Button btWhiteRemote;
    private SeekBar skDifficulty;
    private TextView tvDifficulty;
    private TextView tvHandicap;
    private SeekBar skHandicap;

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.btWhiteHuman) {
            white = 0;
        } else if (id == R.id.btWhiteComputer) {
            white = 1;
        } else if (id == R.id.btWhiteRemote) {
            white = 2;
        } else if (id == R.id.btBlackHuman) {
            black = 0;
        } else if (id == R.id.btBlackComputer) {
            black = 1;
        } else if (id == R.id.btBlackRemote) {
            black = 2;
        }
        setWhite();
        setBlack();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_new_game);

        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);

        tvDifficulty = findViewById(R.id.tvDifficulty);
        tvDifficulty.setText(MessageFormat.format(
                getString(R.string.computerStrength), settings.getInt(
                        "strength", 1)));
        tvHandicap = findViewById((R.id.tvHandicap));
        tvHandicap.setText((MessageFormat.format(getString(R.string.handicap),
                settings.getInt("handicap", 0))));
        // set up buttons
        btWhiteHuman = findViewById(R.id.btWhiteHuman);
        btWhiteHuman.setOnClickListener(this);

        btWhiteComputer = findViewById(R.id.btWhiteComputer);
        btWhiteComputer.setOnClickListener(this);

        btWhiteRemote = findViewById(R.id.btWhiteRemote);
        btWhiteRemote.setOnClickListener(this);

        white = settings.getInt("white", 0);

        btBlackHuman = findViewById(R.id.btBlackHuman);
        btBlackHuman.setOnClickListener(this);

        btBlackComputer = findViewById(R.id.btBlackComputer);
        btBlackComputer.setOnClickListener(this);

        btBlackRemote = findViewById(R.id.btBlackRemote);
        btBlackRemote.setOnClickListener(this);

        black = settings.getInt("black", 1);

        setBlack();
        setWhite();

        Button btStart = findViewById(R.id.btNewGamePlay);
        btStart.setOnClickListener(v -> {
            if ((white == 1) && (black == 1)) {
                Toast.makeText(NewGameActivity.this, R.string.compVsCompToast,
                        Toast.LENGTH_SHORT).show();
                return;
            }
            if ((white == 2) && (black == 2)) {
                Toast.makeText(NewGameActivity.this, R.string.remoteVsRemoteToast,
                        Toast.LENGTH_SHORT).show();
                return;
            }

            SharedPreferences settings1 = PreferenceManager
                    .getDefaultSharedPreferences(NewGameActivity.this);
            SharedPreferences.Editor editor = settings1.edit();
            editor.putString("FEN", Position.STARTUP_FEN[skHandicap.getProgress()]);
            editor.putInt("white", white);
            editor.putInt("black", black);
            editor.putBoolean("flipped", false);
            editor.putBoolean("gameEnded", false);
            editor.putInt("strength", skDifficulty.getProgress() + 1);
            editor.putInt("handicap", skHandicap.getProgress() );
            editor.apply();

            finish();
        });

        // set up difficulty bar
        skDifficulty = findViewById(R.id.skDifficulty);
        skDifficulty.setProgress(settings.getInt("strength", 3) - 1);
        skDifficulty.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                tvDifficulty.setText(MessageFormat.format(
                        getString(R.string.computerStrength), progress + 1));
            }
        });
        // set up handicap bar
        skHandicap = findViewById(R.id.skHandicap);
        skHandicap.setProgress(settings.getInt("handicap", 0) );
        skHandicap.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                tvHandicap.setText(MessageFormat.format(
                        getString(R.string.handicap), progress));
            }
        });
    }

    private void setBlack() {
        if (black == 0) {
            btBlackHuman.setBackgroundResource(R.drawable.btn_left_toggled);
            btBlackComputer.setBackgroundResource(R.drawable.btn_right);
            btBlackRemote.setBackgroundResource(R.drawable.btn_right);
        } else if (black == 1) {
            btBlackHuman.setBackgroundResource(R.drawable.btn_left);
            btBlackComputer.setBackgroundResource(R.drawable.btn_right_toggled);
            btBlackRemote.setBackgroundResource(R.drawable.btn_right);
        } else if (black == 2) {
            btBlackHuman.setBackgroundResource(R.drawable.btn_left);
            btBlackComputer.setBackgroundResource(R.drawable.btn_right);
            btBlackRemote.setBackgroundResource(R.drawable.btn_right_toggled);
        }
    }

    private void setWhite() {
        if (white == 0) {
            btWhiteHuman.setBackgroundResource(R.drawable.btn_left_toggled);
            btWhiteComputer.setBackgroundResource(R.drawable.btn_right);
            btWhiteRemote.setBackgroundResource(R.drawable.btn_right);
        } else if (white == 1) {
            btWhiteHuman.setBackgroundResource(R.drawable.btn_left);
            btWhiteComputer.setBackgroundResource(R.drawable.btn_right_toggled);
            btWhiteRemote.setBackgroundResource(R.drawable.btn_right);
        } else if (white == 2) {
            btWhiteHuman.setBackgroundResource(R.drawable.btn_left);
            btWhiteComputer.setBackgroundResource(R.drawable.btn_right);
            btWhiteRemote.setBackgroundResource(R.drawable.btn_right_toggled);
        }
    }

}
